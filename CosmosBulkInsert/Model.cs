﻿using Newtonsoft.Json;
using System;

namespace CosmosBulkInsert
{
    class Model
    {
        public Model(string[] values)
        {
            MobileId = values[0];
            Date = values[1];
            DateTime = DateTime.Parse(values[1]);
            Latitude = double.Parse(values[2]);
            Longitude = double.Parse(values[3]);
        }

        [JsonProperty(PropertyName = "DATE")]
        public string Date { get; set; }

        [JsonProperty(PropertyName = "DATETIME")]
        public DateTime DateTime { get; set; }

        [JsonProperty(PropertyName = "LONGITUDE")]
        public double Longitude { get; set; }

        [JsonProperty(PropertyName = "LATITUDE")]
        public double Latitude { get; set; }

        [JsonProperty(PropertyName = "MOBILE_ID")]
        public string MobileId { get; set; }
    }
}
