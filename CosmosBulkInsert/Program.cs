﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CosmosBulkInsert
{
    public class Program
    {
        private static DocumentClient client;

        //Assign a id for your database & collection 
        private static readonly string databaseId = ConfigurationManager.AppSettings["DatabaseId"];
        private static readonly string collectionId = ConfigurationManager.AppSettings["CollectionId"];

        //Read the DocumentDB endpointUrl and authorisationKeys from config
        //These values are available from the Azure Management Portal on the DocumentDB Account Blade under "Keys"
        //NB > Keep these values in a safe & secure location. Together they provide Administrative access to your DocDB account
        private static readonly string endpointUrl = ConfigurationManager.AppSettings["EndPointUrl"];
        private static readonly string authorizationKey = ConfigurationManager.AppSettings["AuthorizationKey"];

        private static readonly string dataPath = ConfigurationManager.AppSettings["DataPath"];
        private static readonly int maxThreads = int.Parse(ConfigurationManager.AppSettings["MaxThreads"]);

        public static void Main(string[] args)
        {
            try
            {
                //Get a Document client
                using (client = new DocumentClient(new Uri(endpointUrl), authorizationKey))
                {
                    RunDemoAsync(databaseId, collectionId, args).Wait();
                }
            }
            catch (DocumentClientException de)
            {
                Exception baseException = de.GetBaseException();
                Console.WriteLine("{0} error occurred: {1}, Message: {2}", de.StatusCode, de.Message, baseException.Message);
            }
            catch (Exception e)
            {
                Exception baseException = e.GetBaseException();
                Console.WriteLine("Error: {0}, Message: {1}", e.Message, baseException.Message);
            }
            finally
            {
                Console.WriteLine("End of demo, press any key to exit.");
                //Console.ReadKey();
            }
        }

        private static async Task RunDemoAsync(string databaseId, string collectionId, string[] args)
        {
            //Get, or Create, the Database
            Database database = await GetOrCreateDatabaseAsync(databaseId);

            //Get, or Create, the Document Collection
            DocumentCollection collection = await GetOrCreateCollectionAsync(database.SelfLink, collectionId);

            var pattern = args.FirstOrDefault();
            //foreach (var file in Directory.GetFiles(dataPath, pattern))
            Parallel.ForEach(
                Directory.GetFiles(dataPath, pattern),
                new ParallelOptions { MaxDegreeOfParallelism = maxThreads },
                (currentFile) =>
            {
                //Run Bulk Import per file
                RunBulkImport(collection.SelfLink, currentFile).Wait();
            });
        }

        /// <summary>
        /// Import many documents using stored procedure.
        /// </summary>
        private static async Task RunBulkImport(string colSelfLink, string file)
        {
            var start = DateTime.Now;
            int bulkSize = 1000;

            // 3. Create stored procedure for this script.
            string body = File.ReadAllText(@".\JS\BulkImport.js");
            StoredProcedure sproc = new StoredProcedure
            {
                Id = "BulkImport" + Guid.NewGuid(),
                Body = body
            };

            await TryDeleteStoredProcedure(colSelfLink, sproc.Id);
            sproc = await client.CreateStoredProcedureAsync(colSelfLink, sproc);

            Console.WriteLine($"Loading CSV file {file} ({DateTime.Now})");
            //var jsonFile = JsonConvert.DeserializeObject<Model[]>(File.ReadAllText(file));
            var csvFile = File.ReadAllLines(file).Select(a => new Model(a.Split(';'))).ToArray();
            Console.WriteLine($"CSV file {file} Loaded ({DateTime.Now})");
            var objCount = csvFile.Count();
            int currentCount = 0;
            // 6. execute the batch.
            Console.WriteLine($"Batching with {bulkSize} ({DateTime.Now})");
            while (currentCount < objCount)
            {
                var now = DateTime.Now;
                var toInserts = csvFile.Skip(currentCount).Take(bulkSize);
                try
                {
                    StoredProcedureResponse<int> scriptResult = await client.ExecuteStoredProcedureAsync<int>(
                        sproc != null ? sproc.SelfLink : "BulkImport",
                        new RequestOptions { PartitionKey = new PartitionKey(toInserts.FirstOrDefault().Date) },
                        toInserts
                    );
                    // 7. Prepare for next batch.
                    int currentlyInserted = scriptResult.Response;
                    Console.WriteLine($"[{file}] Inserted: {currentlyInserted} in {(DateTime.Now - now).TotalSeconds}");
                    currentCount += currentlyInserted;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"[{file}] Error: {e.Message} - Sleeping 2s");
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                }
            }

            Console.WriteLine($"[{file}] Bulk done for - inserted {currentCount} objects in [{(DateTime.Now - start).TotalSeconds} seconds]");
            await TryDeleteStoredProcedure(colSelfLink, sproc.Id);
        }

        /// <summary>
        /// Creates the script for insertion
        /// </summary>
        /// <param name="currentIndex">the current number of documents inserted. this marks the starting point for this script</param>
        /// <param name="maxScriptSize">the maximum number of characters that the script can have</param>
        /// <returns>Script as a string</returns>
        private static string CreateBulkInsertScriptArguments(string[] docFileNames, int currentIndex, int maxCount, int maxScriptSize)
        {
            var jsonDocumentArray = new StringBuilder();
            jsonDocumentArray.Append("[");

            if (currentIndex >= maxCount) return string.Empty;
            jsonDocumentArray.Append(File.ReadAllText(docFileNames[currentIndex]));

            int scriptCapacityRemaining = maxScriptSize;
            string separator = string.Empty;

            int i = 1;
            while (jsonDocumentArray.Length < scriptCapacityRemaining && (currentIndex + i) < maxCount)
            {
                jsonDocumentArray.Append(", " + File.ReadAllText(docFileNames[currentIndex + i]));
                i++;
            }

            jsonDocumentArray.Append("]");
            return jsonDocumentArray.ToString();
        }

        /// <summary>
        /// If a Stored Procedure is found on the DocumentCollection for the Id supplied it is deleted
        /// </summary>
        /// <param name="colSelfLink">DocumentCollection to search for the Stored Procedure</param>
        /// <param name="sprocId">Id of the Stored Procedure to delete</param>
        /// <returns></returns>
        private static async Task TryDeleteStoredProcedure(string colSelfLink, string sprocId)
        {
            StoredProcedure sproc = client.CreateStoredProcedureQuery(colSelfLink).Where(s => s.Id == sprocId).AsEnumerable().FirstOrDefault();
            if (sproc != null)
            {
                await client.DeleteStoredProcedureAsync(sproc.SelfLink);
            }
        }

        /// <summary>
        /// If a Trigger is found on the DocumentCollection for the Id supplied it is deleted
        /// </summary>
        /// <param name="colSelfLink">DocumentCollection to search for the Trigger</param>
        /// <param name="triggerId">Id of the Trigger to delete</param>
        /// <returns></returns>
        private static async Task TryDeleteTrigger(string colSelfLink, string triggerId)
        {
            Trigger trigger = client.CreateTriggerQuery(colSelfLink).Where(t => t.Id == triggerId).AsEnumerable().FirstOrDefault();
            if (trigger != null)
            {
                await client.DeleteTriggerAsync(trigger.SelfLink);
            }
        }

        /// <summary>
        /// If a UDF is found on the DocumentCollection for the Id supplied it is deleted
        /// </summary>
        /// <param name="colSelfLink">DocumentCollection to search for the UDF</param>
        /// <param name="udfId">Id of the UDF to delete</param>
        /// <returns></returns>
        private static async Task TryDeleteUDF(string colSelfLink, string udfId)
        {
            UserDefinedFunction udf = client.CreateUserDefinedFunctionQuery(colSelfLink).Where(u => u.Id == udfId).AsEnumerable().FirstOrDefault();
            if (udf != null)
            {
                await client.DeleteUserDefinedFunctionAsync(udf.SelfLink);
            }
        }

        /// <summary>
        /// Get a DocumentCollection by id, or create a new one if one with the id provided doesn't exist.
        /// </summary>
        /// <param name="id">The id of the DocumentCollection to search for, or create.</param>
        /// <returns>The matched, or created, DocumentCollection object</returns>
        private static async Task<DocumentCollection> GetOrCreateCollectionAsync(string dbLink, string id)
        {
            DocumentCollection collection = client.CreateDocumentCollectionQuery(dbLink).Where(c => c.Id == id).ToArray().FirstOrDefault();
            if (collection == null)
            {
                collection = new DocumentCollection { Id = id };
                collection.IndexingPolicy.IncludedPaths.Add(new IncludedPath
                {
                    Path = "/*",
                    Indexes = new Collection<Index>(new Index[]
                    {
                        new RangeIndex(DataType.Number) { Precision = -1},
                        new RangeIndex(DataType.String) { Precision = -1},
                    }),
                });

                collection = await client.CreateDocumentCollectionAsync(dbLink, collection);
            }

            return collection;
        }

        /// <summary>
        /// Get a Database by id, or create a new one if one with the id provided doesn't exist.
        /// </summary>
        /// <param name="id">The id of the Database to search for, or create.</param>
        /// <returns>The matched, or created, Database object</returns>
        private static async Task<Database> GetOrCreateDatabaseAsync(string id)
        {
            Database database = client.CreateDatabaseQuery().Where(db => db.Id == id).ToArray().FirstOrDefault();
            if (database == null)
            {
                database = await client.CreateDatabaseAsync(new Database { Id = id });
            }

            return database;
        }
    }
}
